require('app-module-path').addPath(__dirname);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uuid = require('uuid').v4;
const bcrypt = require('bcrypt');
const enums = require('models/Enums.js');

const schema = new Schema({
    userSessionUUID: {type: String, required: true, index: true, unique: true, default: uuid},
    userUUID: {type: String, index: true},
    sessionToken: {type: String, index: true},
    sessionEstablished: {type: Boolean, index: true, required: true},
    sessionEnded: {type: Date, index: true},
    sessionFailureReason: {type: String, enums: Object.values(enums.SessionFailures)},
    loginNameProvided: {type: String, required: true},
    sessionRequests: {type: Number, default: 1},
    created: {type: Date, required: true, default: Date.now},
    modified: {type: Date, required: true, default: Date.now},
    disabled: {type: Boolean, required: true, default: false, index: true}
});

const JSONMap = new Map([
    ['userSessionUUID', 'userSessionID'], ['userUUID', 'userID'],
    ['sessionEstablished', 'sessionEstablished'], ['sessionEnded', 'sessionEnded'],
    ['sessionFailureReason', 'sessionFailureReason'], ['loginNameProvided', 'loginNameProvided'],
    ['sessionRequests', 'sessionRequests']
]);

schema.methods.asResponse = function () {
    const response = {};
    JSONMap.forEach( (respKey, modelKey) => {
        response[respKey] = this[modelKey];
    });
    return response;
};

schema.statics.findActiveSession = async function( userUUID ) {
    const activeSession = await this.findOne({ userUUID: userUUID, sessionEnded: null })
        .sort({ sessionEstablished: -1})
        .exec();
    return activeSession;
}

function preProcessUpdate(current, update) {
    update.modified = Date.now();

    if (update.loginNameProvided != null) {
        update.loginNameProvided = update.loginNameProvided.toLowerCase();
    }
}

schema.pre('save', function (next) {
    preProcessUpdate(this, this);
    next();
})

schema.pre('findOneAndUpdate', function (next) {
    preProcessUpdate(this, this.getUpdate());
    next();
})

schema.pre('update', function (next) {
    preProcessUpdate(this, this.getUpdate());
    next();
})

module.exports = mongoose.model('UserSession', schema);

