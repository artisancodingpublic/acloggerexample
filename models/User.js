require('app-module-path').addPath(__dirname);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uuid = require('uuid').v4;
const bcrypt = require('bcrypt');
const enums = require('models/Enums.js');

const schema = new Schema({
    userUUID: {type: String, required: true, index: true, unique: true, default: uuid},
    login: {type: String, index: true},
    password: {type: String, index: true},
    userRole: {type: String, enums: Object.values(enums.UserRoles)},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    email: {type: String, index: true},
    mobile: {type: String, index: true },
    created: {type: Date, required: true, default: Date.now},
    modified: {type: Date, required: true, default: Date.now},
    disabled: {type: Boolean, required: true, default: false, index: true}
});

const JSONMap = new Map([
    ['userUUID', 'id'], ['login', 'login' ], ['userRole', 'userRole'],
    ['firstName', 'firstName'], ['lastName', 'lastName'],
    ['email', 'email'], ['mobile', 'mobile'], ['disabled', 'disabled']
]);

schema.methods.asResponse = function () {
    const response = {};
    JSONMap.forEach( (respKey, modelKey) => {
       response[respKey] = this[modelKey];
    });
    return response;
};

function encryptPassword(password) {
    if( password != null ) {
        const salt = bcrypt.genSaltSync(parseInt(process.env.PASSWORD_SALT_ROUNDS))
        return bcrypt.hashSync(password, salt);
    }
}

function preProcessUpdate(current, update) {
    update.modified = Date.now();

    if (update.password != null &&
        (current.password != update.password || current.isModified('password'))) {
        update.password = encryptPassword(update.password);
    }

    if (update.login != null) {
        update.login = update.login.toLowerCase();
    }

    if (update.email != null) {
        update.email = update.email.toLowerCase();
    }
}

schema.pre('save', function (next) {
    preProcessUpdate(this, this);
    next();
})

schema.pre('findOneAndUpdate', function (next) {
    preProcessUpdate(this, this.getUpdate());
    next();
})

schema.pre('update', function (next) {
    preProcessUpdate(this, this.getUpdate());
    next();
})

module.exports = mongoose.model('User', schema);

