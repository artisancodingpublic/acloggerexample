module.exports = {
    UserRoles: {
        ADMIN: 'Administrator',
        USER: 'User'
    },
    SessionFailures: {
        BAD_PASSWORD: 'Incorrect password',
        USER_DISABLED: 'User disabled',
        USER_NOT_FOUND: 'User not found',
    }
};
