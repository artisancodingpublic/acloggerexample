# AC-Logger
**Author:** John Bentley, II<br>
**Company:** ArtisanCoding

## Description
ACLogger is a wrapper of the popular [Winston](https://www.npmjs.com/package/winston) 
and [Wintson Daily Log Rotate File](https://www.npmjs.com/package/winston-daily-rotate-file) 
to standardize the management of logging, focused on:
- Bolstering security compliance by allowing the environment to set a maximum appropriate log level
- Proving the ability to simultaneously log to human and machine readable formats
- Define logging calls to capture key Express request information and authorization data to better 
correlate log entries
- Separate logging for managing access logs to support security audits and reports



## Options
- logMode - what mode the system is operating in - Production, Test, Development - for 
capping the allowed logLevel
- logLevel - maximum level for log events to be recorded, up to the maximum level allows 
by the logMode 
- humanReadable - settings for the human readable log management
>- logPath - path for storing logs; absolute path or relative from installation directory
>- accessLogPath - path for storing access logs; absolute path or relative from installation directory
>- enabled - true to record humanReadable logs, false to not
>- maximumRetention - how long the logs are retained on the filesystem uses 
>Winston Daily Log Rotate File format, e.g. '5m', '4h', '7d'
- machineReadable - settings for the machine readable log management
>- logPath - path for storing logs; absolute path or relative from installation directory
>- accessLogPath - path for storing access logs; absolute path or relative from installation directory
>- enabled - true to record humanReadable logs, false to not
>- maximumRetention - how long the logs are retained on the filesystem uses 
>Winston Daily Log Rotate File format, e.g. '5m', '4h', '7d'
### Example
```
{
    logMode = ACLogger.LOG_MODES.TEST;
    logLevel = ACLogger.LOG_LEVELS.TRACE;

    humanReadable = {
        logPath: './logs',
        accessLogPath: ',/access-logs',
        enabled: true,
        maxRetention: '7d'
    }

    machineReadable = {
        logPath: './logs/json',
        accessLogPath: ',/access-logs/json',
        enabled: true,
        maxRetention: '14d'
    }
}
```
