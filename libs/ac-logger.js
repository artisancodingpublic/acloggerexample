const {createLogger, format, transports, addColors} = require('winston');
const {combine, timestamp, label, printf, colorize, json} = format;
require('winston-daily-rotate-file');

module.exports = class ACLogger {
    static LOG_LEVELS = {
        ERROR: 0,
        WARN: 1,
        REQUEST: 2,
        TRACE: 3,
        DEBUG: 4,
    }

    static LOG_COLORS = {
        ERROR: 'red',
        WARN: 'yellow',
        REQUEST: 'blue',
        TRACE: 'cyan',
        DEBUG: 'gray',
    }

    logLevel = ACLogger.LOG_LEVELS.WARN;

    static ACCESS_LOG_LEVELS = {
        ACCESS: 1,
        ACCESS_ERROR: 2
    }

    static ACCESS_LOG_COLORS = {
        ACCESS: 'green',
        ACCESS_ERROR: 'red',
    }

    static LOG_MODES = {
        DEVELOPMENT: 'development',
        TEST: 'test',
        PRODUCTION: 'production',
    }

    logMode = ACLogger.LOG_MODES.PRODUCTION;

    humanReadable = {
        logPath: '.',
        accessLogPath: ',',
        enabled: true,
        maxRetention: '14d'
    }

    machineReadable = {
        logPath: '.',
        accessLogPath: ',',
        enabled: true,
        maxRetention: '14d'
    }

    _humanReadableLogger = null;
    _machineReadableLogger = null;
    _accessHumanReadableLogger = null;
    _accessMachineReadableLogger = null;

    constructor(opts) {

        if (opts.logMode != null) {
            this.logMode = opts.logMode;
        }

        ['humanReadable', 'machineReadable'].forEach(target => {
            if (opts[target] == null) return;

            if (opts[target].logPath != null) {
                if (opts[target].logPath.endsWith('/')) {
                    this[target].logPath = this[target].accessLogPath = opts[target].logPath
                        .substr(0, opts[target].logPath.length - 1);
                } else {
                    this[target].logPath = this[target].accessLogPath = opts[target].logPath;
                }
            }

            if (opts[target].accessLogPath != null) {
                if (opts[target].accessLogPath.endsWith('/')) {
                    this[target].accessLogPath = opts[target].accessLogPath
                        .substr(0, opts[target].accessLogPath.length - 1);
                } else {
                    this[target].accessLogPath = opts[target].accessLogPath;
                }
            }

            if (opts[target].enabled != null) {
                this[target].enabled = opts[target].enabled;
            }

            if (opts[target].maxRetention != null) {
                this[target].maxRetention = opts[target].maxRetention;
            }


        });

        if (opts.logLevel != null) {
            this.logLevel = opts.logLevel;
        }

        switch (this.logMode) {
            case ACLogger.LOG_MODES.PRODUCTION:
                this.logLevel = Math.min(ACLogger.LOG_LEVELS.WARN, this.logLevel);
                break;
            case ACLogger.LOG_MODES.TEST:
                this.logLevel = Math.min(Math.max(ACLogger.LOG_LEVELS.REQUEST, this.logLevel), ACLogger.LOG_LEVELS.TRACE);
                break;
            case ACLogger.LOG_MODES.DEVELOPMENT:
                this.logLevel = Math.min(Math.max(ACLogger.LOG_LEVELS.TRACE, this.logLevel), ACLogger.LOG_LEVELS.DEBUG);
                break;
        }

        if (this.humanReadable.enabled) {
            this._createHumanReadableLogger();
            this._createAccessHumanReadableLogger();
        }

        if (this.machineReadable.enabled) {
            this._createMachineReadableLogger();
            this._createAccessMachineReadableLogger();
        }
    }

    _createHumanReadableTransports() {
        const transportList = [];
        for (let key in ACLogger.LOG_LEVELS) {
            if (ACLogger.LOG_LEVELS[key] <= this.logLevel) {
                transportList.push(
                    new (transports.DailyRotateFile)({
                        name: key,
                        filename: `${this.humanReadable.logPath}/${key}-%DATE%.log`,
                        datePattern: 'YYYYMMDD',
                        zippedArchive: true,
                        level: key,
                        maxFiles: this.humanReadable.maxRetention
                    }))
            }
        }

        return transportList;
    }

    _createHumanReadableLogger() {
        addColors(ACLogger.LOG_COLORS);
        this._humanReadableLogger = createLogger({
            levels: ACLogger.LOG_LEVELS,
            format: combine(
                colorize(),
                timestamp(),
                printf(info => {
                    return `${info.timestamp} [${info.level}]: ${info.message}`;
                })
            ),
            transports: this._createHumanReadableTransports()
        });
    }

    _createMachineReadableTransports() {
        const transportList = [];
        for (let key in ACLogger.LOG_LEVELS) {
            if (ACLogger.LOG_LEVELS[key] <= this.logLevel) {
                transportList.push(
                    new (transports.DailyRotateFile)({
                        name: key,
                        filename: `${this.machineReadable.logPath}/${key}-%DATE%.json`,
                        datePattern: 'YYYYMMDD',
                        zippedArchive: true,
                        level: key,
                        maxFiles: this.humanReadable.maxRetention
                    }))
            }
        }

        return transportList;
    }

    _createMachineReadableLogger() {
        this._machineReadableLogger = createLogger({
            levels: ACLogger.LOG_LEVELS,
            format: combine(
                timestamp(),
                format.json()
            ),
            transports: this._createMachineReadableTransports()
        });
    }

    _createAccessHumanReadableTransports() {
        return [
            new (transports.DailyRotateFile)({
                name: 'ACCESS',
                filename: `${this.humanReadable.accessLogPath}/ACCESS-%DATE%.log`,
                datePattern: 'YYYYMMDD',
                zippedArchive: true,
                level: 'ACCESS_ERROR',
                maxFiles: this.humanReadable.maxRetention
            })
        ];

    }

    _createAccessHumanReadableLogger() {
        addColors(ACLogger.ACCESS_LOG_COLORS);
        this._accessHumanReadableLogger = createLogger({
            levels: ACLogger.ACCESS_LOG_LEVELS,
            format: combine(
                colorize(),
                timestamp(),
                printf(info => {
                    return `${info.timestamp} [${info.level}]: ${info.message}`;
                })
            ),
            transports: this._createAccessHumanReadableTransports()
        });
    }

    _createAccessMachineReadableTransports() {
        return [
            new (transports.DailyRotateFile)({
                name: 'ACCESS',
                filename: `${this.machineReadable.accessLogPath}/ACCESS-%DATE%.json`,
                datePattern: 'YYYYMMDD',
                zippedArchive: true,
                level: 'ACCESS_ERROR',
                maxFiles: this.machineReadable.maxRetention
            })
        ];

    }

    _createAccessMachineReadableLogger() {
        this._accessMachineReadableLogger = createLogger({
            levels: ACLogger.ACCESS_LOG_LEVELS,
            format: combine(
                timestamp(),
                format.json()
            ),
            transports: this._createAccessMachineReadableTransports()
        });
    }

    get humanReadableLogger() {
        return this._humanReadableLogger;
    }

    get accessHumanReadableLogger() {
        return this._accessHumanReadableLogger;
    }

    get machineReadableLogger() {
        return this._machineReadableLogger;
    }

    get accessMachineReadableLogger() {
        return this._accessMachineReadableLogger;
    }

    _logHumanReadableEntry(level, req, accessIdentity, description = null, dataObject = null) {
        if (this.humanReadable.enabled) {

            if (level <= this.logLevel) {
                const query = [];
                for (let queryName in req.query) {
                    query.push(`${queryName}=${JSON.stringify(req.query[queryName])}`);
                }
                let msg = `${req.baseUrl}${req.path} ${req.method} {${query.join(',')}} ${req.ip} ${JSON.stringify(accessIdentity)}`;
                if (description != null) {
                    msg += ` ${JSON.stringify(description)}`;
                }
                if (dataObject != null) {
                    msg += ` ${JSON.stringify(dataObject)}`;
                }
                const levelArray = Object.entries(ACLogger.LOG_LEVELS).find(ea => ea[1] === level);
                this.humanReadableLogger.log({
                    level: levelArray[0],
                    message: msg
                })
            }
        }
    }

    _logMachineReadableEntry(level, req, accessIdentity, description = null, dataObject = null) {
        if (this.machineReadable.enabled) {
            if (level <= this.logLevel) {
                const query = [];
                for (let queryName in req.query) {
                    query.push([queryName, `${JSON.stringify(req.query[queryName])}`]);
                }
                const request = {
                    ip: req.ip,
                    baseUrl: req.baseUrl,
                    path: req.path,
                    method: req.method,
                    query: query
                }
                const levelArray = Object.entries(ACLogger.LOG_LEVELS).find(ea => ea[1] === level);
                const msg = {
                    level: levelArray[0],
                    request: request,
                    accessIdentity: accessIdentity,
                    description: description,
                    data: dataObject
                }
                this.machineReadableLogger.log(msg);
            }
        }
    }

    _logAccessHumanReadableEntry(level, req, accessIdentity, description = null) {
        if (this.humanReadable.enabled) {
            const query = [];
            for (let queryName in req.query) {
                query.push(`${queryName}=${JSON.stringify(req.query[queryName])}`);
            }
            let msg = `${req.baseUrl}${req.path} ${req.method} {${query.join(',')}} ${req.ip} ${JSON.stringify(accessIdentity)}`;
            if (description != null) {
                msg += ` ${JSON.stringify(description)}`;
            }
            const levelArray = Object.entries(ACLogger.ACCESS_LOG_LEVELS).find(ea => ea[1] === level);
            this.accessHumanReadableLogger.log({
                level: levelArray[0],
                message: msg
            })
        }
    }

    _logAccessMachineReadableEntry(level, req, accessIdentity, description = null, dataObject = null) {
        if (this.machineReadable.enabled) {
            const query = [];
            for (let queryName in req.query) {
                query.push([queryName, `${JSON.stringify(req.query[queryName])}`]);
            }
            const request = {
                ip: req.ip,
                baseUrl: req.baseUrl,
                path: req.path,
                method: req.method,
                query: query
            }
            const levelArray = Object.entries(ACLogger.ACCESS_LOG_LEVELS).find(ea => ea[1] === level);
            const msg = {
                level: levelArray[0],
                request: request,
                accessIdentity: accessIdentity,
                description: description,
                data: dataObject
            }
            this.accessMachineReadableLogger.log(msg);
        }
    }

    error(req, accessIdentity, error) {
        this._logHumanReadableEntry(ACLogger.LOG_LEVELS.ERROR, req, accessIdentity, error.toString(), error);
        this._logMachineReadableEntry(ACLogger.LOG_LEVELS.ERROR, req, accessIdentity, error.toString(), error);
    }

    warn(req, accessIdentity, description = null) {
        this._logHumanReadableEntry(ACLogger.LOG_LEVELS.WARN, req, accessIdentity, description);
        this._logHachineReadableEntry(ACLogger.LOG_LEVELS.WARN, req, accessIdentity, description);
    }

    request(req, accessIdentity, description = null) {
        this._logHumanReadableEntry(ACLogger.LOG_LEVELS.REQUEST, req, accessIdentity, description);
        this._logMachineReadableEntry(ACLogger.LOG_LEVELS.REQUEST, req, accessIdentity, description);
    }

    trace(req, accessIdentity, description) {
        this._logHumanReadableEntry(ACLogger.LOG_LEVELS.TRACE, req, accessIdentity, description);
        this._logMachineReadableEntry(ACLogger.LOG_LEVELS.TRACE, req, accessIdentity, description);
    }

    debug(req, accessIdentity, description, dataObject) {
        this._logHumanReadableEntry(ACLogger.LOG_LEVELS.DEBUG, req, accessIdentity, description, dataObject);
        this._logMachineReadableEntry(ACLogger.LOG_LEVELS.DEBUG, req, accessIdentity, description, dataObject);
    }

    access(req, accessIdentity, description) {
        this._logAccessHumanReadableEntry(ACLogger.ACCESS_LOG_LEVELS.ACCESS, req, accessIdentity, description);
        this._logAccessMachineReadableEntry(ACLogger.ACCESS_LOG_LEVELS.ACCESS, req, accessIdentity, description);
    }

    accessError(req, accessIdentity, description) {
        this._logAccessHumanReadableEntry(ACLogger.ACCESS_LOG_LEVELS.ACCESS_ERROR, req, accessIdentity, description);
        this._logAccessMachineReadableEntry(ACLogger.ACCESS_LOG_LEVELS.ACCESS_ERROR, req, accessIdentity, description);
    }
}
