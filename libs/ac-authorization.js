require('dotenv').config();
Promise = require('bluebird');
const jwt = require('jsonwebtoken');
const crypto = require('crypto-js');

class ACAuthorization {
    identifier;
    secret;
    expiry;

    constructor( identifier, secret, expiry = null ) {
        this.identifier = identifier;
        this.secret = secret;
        this.expiry = expiry;
    }

    createToken(tokenData = null) {

        const token = jwt.sign(tokenData, this.secret, {
            expiresIn: this.expiry,
            issuer: this.identifier
        });

        return token;
    }

    verifyToken(token) {
        "use strict";
        let payload = null;

        if (jwt.verify(token, this.secret, { issuer: this.identifier} ) ) {
            const decode = jwt.decode(token, { complete: true, json: true});
            if( decode != null ) {
                payload = decode.payload;
            }
        } else {
            const err = new Error('Unauthorized');
            err.status = 401;
            throw err;
        }

        return payload;
    }

    createKey() {
        return crypto.AES.encrypt(this.identifier, this.secret).toString(crypto.enc.Utf8);
    }

    verifyKey( key ) {
        try {
            if( this.identifier === crypto.AES.decrypt(key, this.secret).toString(crypto.enc.Utf8) ) {
                return this.identifier
            } else {
                return null;
            }
        } catch( err ) {
            return null;
        }
    }
}


module.exports = ACAuthorization;
