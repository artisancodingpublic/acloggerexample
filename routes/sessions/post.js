require('app-module-path').addPath(__dirname);
const logger = require('common/logger');
const authorization = require('common/authorization');

const bcrypt = require('bcrypt');
const Enums = require('models/Enums');
const User = require('models/User');
const UserSession = require('models/UserSession');


/* Operation postSession */
module.exports = async function (req, res, next) {
    let userSessionData = null;
    try {

        logger.trace(req, req.authorization, 'Verifying application id is being used.');
        if (req.authorization == null || req.authorization.applicationID == null) {
            logger.trace(req, req.authorization, 'Generating authorization error.');
            const error = new Error('Unauthorized');
            error.status = 401;
            throw error;
        }

        userSessionData = {
            loginNameProvided: req.body.login
        }

        logger.trace(req, req.authorization, 'Search for the active user login as login id, email or mobile,');
        const userQueryCriteria = {
            $or: [
                {login: req.body.login.toLowerCase()},
                {email: req.body.login.toLowerCase()},
                {mobile: req.body.login}
            ],
        }

        logger.trace(req, req.authorization, 'Select users based on criteria.');
        logger.debug(req, req.authorization, 'User Query Criteria', userQueryCriteria);
        const user = await User.findOne(userQueryCriteria).exec();

        logger.debug(req, req.authorization, 'Found User', user);

        logger.trace(req, req.authorization, 'Check for valid user.');
        if (user == null || !bcrypt.compareSync(req.body.password, user.password)) {
            if( user != null ) {
                userSessionData.userUUID = user.userUUID;
                if( user.disabled ) {
                    userSessionData.sessionFailureReason = Enums.SessionFailures.USER_DISABLED;
                } else {
                    userSessionData.sessionFailureReason = Enums.SessionFailures.BAD_PASSWORD;
                }
            } else {
                userSessionData.sessionFailureReason = Enums.SessionFailures.USER_NOT_FOUND
            }
            const error = new Error('Invalid login or password.');
            error.status = 404;
            throw error;
        }

        userSessionData.userUUID = user.userUUID;
        userSessionData.sessionEstablished = true;
        logger.trace(req, req.authorization, 'Create a new user session.');
        logger.debug(req, req.authorization, 'New User Session Data.', userSessionData);
        const userSession = await UserSession.create(userSessionData);
        logger.debug(req, req.authorization, 'New User Session Created.', userSession);

        logger.trace(req, req.authorization, 'Creating Payload for token.');
        const payload = {};
        payload.userID = user.userUUID;
        payload.userRole = user.userRole;
        payload.userSessionID = userSession.userSessionUUID;
        logger.debug(req, req.authorization, 'Payload Token Created.', payload);

        logger.trace(req, req.authorization, 'Creating Response.');
        const response = {
            token: authorization.userAuthorization.createToken(payload),
            user: user.asResponse()
        }
        logger.debug(req, req.authorization, 'Response Created.', payload);

        logger.trace(req, req.authorization, 'Update with User Session with Token.');
        userSession.sessionToken = response.token;
        await userSession.save();
        logger.debug(req, req.authorization, 'Updated User Session with Token.', userSession);

        res.set('Content-Type', 'application/json');
        res.status(201);
        res.json(response);
        return response;
    } catch (err) {
        if( userSessionData == null ) {
            userSessionData.sessionEstablished = false;
            logger.trace(req, req.authorization, 'Create a failed user session.');
            const userSession = await UserSession.create(userSessionData);
            logger.debug(req, req.authorization, 'Failed User Session Created.', userSession);
        }
        logger.error(req, req.authorization, err);
        logger.debug(req, req.authorization, 'Error Stack', err.stack);
        next(err);
    }
}
