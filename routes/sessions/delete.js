require('app-module-path').addPath(__dirname);
require('dotenv').config();
const express = require('express');
const router = express.Router();
const logger = require('common/logger');

const Enums = require('models/Enums');
const UserSession = require('models/UserSession');

/* GET user by ID, login name or current users designated by "me" */
module.exports = async function(req, res, next) {
    try {

        logger.trace(req, req.authorization, 'Verifying user session is being used.');
        if (req.authorization.userID == null) {
            logger.trace(req, req.authorization, 'Generating authorization error.');
            const err = new Error('Unauthorized');
            err.status = 409;
            throw err;
        }

        const sessionQueryCriteria = {
            sessionEstablished: true,
            sessionEnded: null
        };

        logger.trace(req, req.authorization, 'Setting query limits if not an admin.');
        if (req.authorization.userRole != Enums.UserRoles.ADMIN) {
            sessionQueryCriteria.userUUID = req.authorization.userID;
        }

        logger.trace(req, req.authorization, 'Check for special identifier for current.');
        logger.debug(req, req.authorization, 'ID Param', req.params.id );
        if (req.params.id == 'current') {
            logger.trace(req, req.authorization, "Setting 'current' to current user session id.");
            sessionQueryCriteria.userSessionUUID = req.authorization.userSessionID;
        }

        logger.trace(req, req.authorization, 'Find user session based on criteria.');
        logger.debug(req, req.authorization, 'User Query Criteria', sessionQueryCriteria);
        const userSession = await UserSession.findOne(sessionQueryCriteria).exec();

        logger.trace(req, req.authorization, 'Checking if user session was found.');
        logger.debug(req, req.authorization, 'Found user session', userSession);
        if (userSession == null) {
            logger.trace(req, req.authorization, 'Generating not found error.');
            const err = new Error('Session not found');
            err.status = 404;
            throw err;
        }

        logger.trace(req, req.authorization, `Update User Session`);
        userSession.sessionEnded = new Date();
        userSession.sessionRequests++;
        userSession.save();
        logger.debug(req, req.authorization, 'Saved user session', userSession);

        logger.trace(req, req.authorization, `Creating Response`);
        const response = userSession.asResponse();

        logger.trace(req, req.authorization, `Created Response`);
        logger.debug(req, req.authorization, 'Response', response);

        res.contentType('application/json');
        res.status(200);
        res.json(response);
        next();
    } catch (err) {
        logger.error(req, req.authorization, err);
        logger.debug(req, req.authorization, 'Error Stack', err.stack);
        next(err);
    }
}

