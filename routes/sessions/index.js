require('app-module-path').addPath(__dirname);
const express = require('express');
const router = express.Router({ mergeParams: true });

const postSession = require('./post');
const deleteSession = require('./delete');


/* Operation postSession */
router.post('/', postSession);

/* Operation deleteSession - id is the User Session ID or User ID or current which is the session id in the token */
router.delete('/:id', deleteSession);

module.exports = router;
