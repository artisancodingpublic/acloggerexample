require('app-module-path').addPath(__dirname);
require('dotenv').config();
const express = require('express');
const router = express.Router({ mergeParams: true });

/* GET users listing. */
router.get('/', require('./getAll') );

/* GET user by ID, login name or current users designated by "me" */
router.get('/:id', require('./getByID') );

module.exports = router;
