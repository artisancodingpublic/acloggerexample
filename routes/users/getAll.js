require('app-module-path').addPath(__dirname);
require('dotenv').config();
const logger = require('common/logger');

const Enums = require('models/Enums');
const User = require('models/User');

/* GET users listing. */
module.exports = async function (req, res, next) {
    try {

        logger.trace(req, req.authorization, 'Verifying user session is being used.');
        if (req.authorization.userID == null) {
            logger.trace(req, req.authorization, 'Generating authorization error.');
            const err = new Error('Unauthorized');
            err.status = 409;
            throw err;
        }

        const userQueryCriteria = {};

        logger.trace(req, req.authorization, 'Setting query limits if not an admin.');
        if (req.authorization.userRole != Enums.UserRoles.ADMIN) {
            userQueryCriteria.userUUID = req.authorization.userID;
        }

        logger.trace(req, req.authorization, 'Only include disabled if requested.');
        if (req.query.includeDisabled != 'true') {
            logger.trace(req, req.authorization, 'Disabled users not included.');
            userQueryCriteria.disabled = false;
        }

        logger.trace(req, req.authorization, 'Select users based on criteria.');
        logger.debug(req, req.authorization, 'User Query Criteria', userQueryCriteria);
        const users = await User.find(userQueryCriteria).exec();

        logger.trace(req, req.authorization, `Found ${users.length}.`);
        logger.debug(req, req.authorization, 'Found users', users);


        logger.trace(req, req.authorization, `Creating Response`);
        const response = {
            users: users.map(ea => ea.asResponse()),
        }

        logger.trace(req, req.authorization, `Created Response`);
        logger.debug(req, req.authorization, 'Response', response);

        res.contentType('application/json');
        res.status(200);
        res.json(response);
        next();
    } catch (err) {
        logger.error(req, req.authorization, err);
        logger.debug(req, req.authorization, 'Error Stack', err.stack);
        next(err);
    }
}
