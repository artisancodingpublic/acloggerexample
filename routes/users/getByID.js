require('app-module-path').addPath(__dirname);
require('dotenv').config();
const logger = require('common/logger');

const Enums = require('models/Enums');
const User = require('models/User');

/* GET user by ID, login name or current users designated by "me" */
module.exports = async function(req, res, next) {
    try {

        logger.trace(req, req.authorization, 'Verifying user session is being used.');
        if (req.authorization.userID == null) {
            logger.trace(req, req.authorization, 'Generating authorization error.');
            const err = new Error('Unauthorized');
            err.status = 409;
            throw err;
        }

        const userQueryCriteria = {};

        logger.trace(req, req.authorization, 'Setting query limits if not an admin.');
        if (req.authorization.userRole != Enums.UserRoles.ADMIN) {
            userQueryCriteria.userUUID = req.authorization.userID;
        }

        logger.trace(req, req.authorization, 'Check for special identifier for me.');
        logger.debug(req, req.authorization, 'ID Param', req.params.id );
        if (req.params.id == 'me') {
            logger.trace(req, req.authorization, "Setting 'me' to current user's id.");
            userQueryCriteria.userUUID = req.authorization.userID;
        } else {
            userQueryCriteria.$or = [
                { userUUID: req.params.id },
                { login: req.params.id.toLowerCase() }
            ];
        }

        logger.trace(req, req.authorization, 'Find user based on criteria.');
        logger.debug(req, req.authorization, 'User Query Criteria', userQueryCriteria);
        const user = await User.findOne(userQueryCriteria).exec();

        logger.trace(req, req.authorization, 'Checking if user was found.');
        logger.debug(req, req.authorization, 'Found user', user);
        if (user == null) {
            logger.trace(req, req.authorization, 'Generating not found error.');
            const err = new Error('User not found');
            err.status = 404;
            throw err;
        }

        logger.trace(req, req.authorization, `Creating Response`);
        const response = user.asResponse();

        logger.trace(req, req.authorization, `Created Response`);
        logger.debug(req, req.authorization, 'Response', response);

        res.contentType('application/json');
        res.status(200);
        res.json(response);
        next();
    } catch (err) {
        logger.error(req, req.authorization, err);
        logger.debug(req, req.authorization, 'Error Stack', err.stack);
        next(err);
    }
}

