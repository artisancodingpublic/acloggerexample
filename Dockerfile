FROM node:12

# Create services directory
WORKDIR /usr/src/services

# Install app dependencies
COPY package*.json ./
RUN npm --no-cache install

# Copy app soruce code
COPY . .

# Expose port and start application
EXPOSE 3000
CMD [ "node", "bin/www" ]
