require('app-module-path').addPath(__dirname);
require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const logger = require('common/logger');
const authorization = require('common/authorization');
const UserSession = require('models/UserSession');

const mongoose = require('mongoose');
mongoose.connect(process.env.PRIMARY_DB, {useNewUrlParser: true});

const app = express();

app.use(require('cors')());
app.use(morgan((process.env.NODE_ENV === 'development') ? ('dev') : ('')));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(async function (req, res, next) {
    logger.debug(req, null, 'Request Headers', req.headers);
    let authToken = null;
    try {
        if (req.headers.authorization == null) {
            logger.accessError(req, req.headers.authorization, 'Missing Authorization Header');
            const err = new Error('Invalid authorization header.');
            err.status = 401;
            throw err;
        }

        const authHeader = req.headers.authorization.split(' ');

        if (authHeader[0] != 'Bearer' || authHeader.length != 2) {
            logger.accessError(req, req.headers.authorization, 'Invalid Authorization Header');
            const err = new Error('Invalid authorization header.');
            err.status = 401;
            throw err;
        }

        authToken = authHeader[1];

        logger.trace(req, null, 'Authorization Check - Application ID');
        logger.debug(req, null, 'Authorization Check - Application ID', authToken);

        req.authorization = {};
        req.authorization.applicationID = authorization.applicationAuthorization.verifyKey(authToken);

        logger.trace(req, {applicationID: req.authorization.applicationID}, 'Application Authorization Finished');
        logger.debug(req, {applicationID: req.authorization.applicationID}, 'Application Authorization Payload', req.authorization);

        if (req.authorization.applicationID == null) {

            logger.trace(req, null, 'Authorization Check - User ID');
            logger.debug(req, null, 'Authorization Check - User ID', authToken);

            req.authorization = authorization.userAuthorization.verifyToken(authToken);
            logger.debug(req, null, 'User Token Payload', req.authorization);

            logger.trace(req, {userID: req.authorization.userID}, 'User Authorization Passed');
            logger.debug(req, {userID: req.authorization.userID}, '', req.authorization);
            logger.access(req, req.authorization.userID, "User Authorization Accepted");
        } else {
            logger.access(req, req.authorization.applicationID, "Application Authorization Accepted");
        }


        next();
    } catch (err) {
        logger.trace(req, null, err);
        logger.debug(req, null, '', err.stack);
        const nextErr = new Error('Unauthorized');
        nextErr.status = 401;
        if (err.name === 'TokenExpiredError') {
            logger.accessError(req, req.headers.authorization, 'Token Expired');
            nextErr.message = 'Expired';
            await UserSession.updateOne({
                    sessionToken: authToken,
                    sessionEstablished: true,
                    sessionEnded: null
                }, {$set: {sessionEnded: new Date()}}
            );
        } else {
            logger.accessError(req, req.headers.authorization, 'Invalid Token');
        }
        logger.request(req, req.headers.authorization, `Unauthorized Request`);
        next(nextErr);
    }
});

app.use(async function (req, res, next) {
    try {
        if (req.authorization.userID != null) {
            logger.trace(req, req.authorization, 'User authorization - finding User Session');
            const userSession = await UserSession.findOne({
                userSessionUUID: req.authorization.userSessionID,
                userUUID: req.authorization.userID,
                sessionEstablished: true
            });
            logger.debug(req, req.authorization, 'Found User Session', userSession);

            if (userSession == null) {
                logger.error(req, req.authorization, 'User Session Not Found');
                const err = new Error('Unauthorized');
                err.status = 401;
                next(err);
            } else if (userSession.sessionEnded != null) {
                logger.accessError(req, req.authorization, 'User Session Expired');
                logger.error(req, req.authorization, 'User Session Expired');
                const err = new Error('Expired');
                err.status = 401;
                throw(err);
            }

            userSession.sessionRequests++;
            await userSession.save();
        }
        next();
    } catch (err) {
        next(err);
    }
});

app.use(function (req, res, next) {
    logger.request(req, {
        userID: req.authorization.userID,
        applicationID: req.authorization.applicationID
    });
    next();
});

['', 'users', 'sessions'].forEach(route => {
    app.use(`/${route}`, require(`routes/${route}`));
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    if (!res.headersSent) {
        const err = new Error('Not Found');
        err.status = 404;
        next(err);
    } else {
        logger.request(req, {
                userID: req.authorization.userID,
                applicationID: req.authorization.applicationID,
            },
            `Request Completed with Status ${res.statusCode}`
        );
    }
});


// error handler
app.use(function (err, req, res, next) {
    logger.error(req, req.authorization, err);
    logger.debug(req, req.authorization, err, err.stack);

    // render the error page
    res.status(err.status || 500);
    res.set('Content-Type', 'application/json');
    res.json({
        errorCode: err.status,
        errorText: err.message
    });
});

module.exports = app;
