/*
 * My logger settings to help simulate a singleton so logging is consistent across the whole system.
 */
require('app-module-path').addPath(__dirname);
const appRoot = require('app-root-path');
const ACLogger = require('libs/ac-logger');

module.exports = new ACLogger({
    logMode: process.env.NODE_ENV,
    logLevel: ACLogger.LOG_LEVELS.DEBUG,
    humanReadable: {
        logPath: `${appRoot.path}/logs`,
        accessLogPath: `${appRoot.path}/logs`
    },
    machineReadable: {
        logPath: `${appRoot.path}/logs/json`,
        accessLogPath: `${appRoot.path}/logs/json`
    },
})

