/*
 * My authorization settings for both application access and user access.
 */
require('app-module-path').addPath(__dirname);
const ACAuthorization = require('libs/ac-authorization');

module.exports = {
    applicationAuthorization: new ACAuthorization(process.env.APPLICATION_ID, process.env.APPLICATION_SECRET),
    userAuthorization: new ACAuthorization(process.env.JWT_ISSUER, process.env.JWT_SECRET, process.env.JWT_EXPIRY)
};

